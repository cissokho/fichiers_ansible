#!/bin/bash
# Script to automatically add our public key on a list of servers
# to remove the pain from typing each time our password
# when we want to access a server.

# [manual] If you want to copy your key to only one server
#   ssh-copy-id -i ~/.ssh/id_rsa.pub SERVER

# Definition of the servers
SERVERS=(
  "13.251.16.110"
  "13.212.231.8"

)

# Make sure we have your password
if [ -z "$1" ]; then
  echo "You must supply your password!"
  echo " ./ssh-copy-id-servers.sh 'PASSWORD'"
  exit
fi

# Export the password into an environment variable
export SSHPASS=$1

# Iterate over all servers
for SERVER in "${SERVERS[@]}"
do
  # Echo the server name
  echo $SERVER

  # Copy our key the first time to allow
  sshpass -e ssh-copy-id -p 56500 -i ~/.ssh/id_rsa.pub -o StrictHostKeyChecking=no $USER@$SERVER || echo "FAILED"

  # Clean the .ssh folder
  ssh -p 56500 $USER@$SERVER 'rm -rf .ssh'

  # Add back our key, as we have remove the former authorized keys, along with the new one!
  sshpass -e ssh-copy-id -p 56500 -i ~/.ssh/id_rsa.pub -o StrictHostKeyChecking=no $USER@$SERVER || echo "FAILED"
done
